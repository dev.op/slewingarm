#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 22:36:28 2020

@author: pi
"""
import pygame
import colours

#pygame.init()
fontsize = 14
font = pygame.font.Font("BackIssuesBB_reg.otf", fontsize)
fontsize_interface = 24
font_interface = pygame.font.Font("BackIssuesBB_reg.otf", fontsize_interface)

fontsize = 14
#font = pygame.font.SysFont('Arial', fontsize, True, False)
#font = pygame.font.Font("BackIssuesBB_reg.otf", fontsize)
#schrift = pygame.font.SysFont('Arial', 14, False, False)
#font_interface = pygame.font.Font("BackIssuesBB_reg.otf", 24)


geschwindigkeit = font.render("Geschwindigkeiten", True, colours.black)

magazin_leer = font.render("Magazin ist leer", True, colours.lgrey)
magazin_voll = font.render("Magazin ist nicht leer", True, colours.lgrey)

btn_start = font.render("Start", True, colours.black)
btn_stop = font.render("Stop", True, colours.black)


input_0_0 = font_interface.render("1Y1", True, colours.black) # Ausschiebezylinder einfahren
input_0_1 = font_interface.render("3Y1", True, colours.black) # Schwenkantrieb zu Magazin
input_0_2 = font_interface.render("3Y2", True, colours.black) # Schwenkantrieb zu Folgestation
input_0_3 = font_interface.render("2Y2", True, colours.black) # "Vakuum" ausblasen
    
input_0_4 = font_interface.render("2Y1", True, colours.black) # Vakuum einschalten
input_0_5 = font_interface.render("P1", True, colours.black) # Meldelampe
input_0_6 = font_interface.render("", True, colours.black)
input_0_7 = font_interface.render("", True, colours.black)
    
    
output_0_0 = font_interface.render(" S0", True, colours.black)
output_0_1 = font_interface.render(" S1", True, colours.black)
output_0_2 = font_interface.render(" B3", True, colours.black)
output_0_3 = font_interface.render("1B1", True, colours.black)

output_0_4 = font_interface.render("1B2", True, colours.black)
output_0_5 = font_interface.render("3S1", True, colours.black)
output_0_6 = font_interface.render("3S2", True, colours.black)
output_0_7 = font_interface.render("2B1", True, colours.black)