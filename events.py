#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 11:44:39 2020

@author: pi
"""
import pygame
#import colours
import RPi.GPIO as GPIO

from gpio_settings import gpio_definition
#from slewingarm import slewingarm as sa


def events(sa, zylindergeschwindigkeit, states, button_rects):
    
    gpio_definition()
    
    btn_magazin = button_rects[0]
    btn_start = button_rects[1]
    btn_stop = button_rects[2]
    
    #slewingarm = sa()
    #slewingarm = saz
    #sa = saz
    m_btn_down = states[0]
    light_beam = states[1]
    position = states[2]
    
    
    slewing_arm_runs = True

    for event in pygame.event.get():
        
        if event.type == pygame.QUIT:
            slewing_arm_runs = False
            print("Spieler hat Quit-Button angeklickt")
            
        elif event.type == pygame.KEYDOWN:
            print("Spieler hat Taste gedrückt")
            if event.key == pygame.K_RIGHT:
                position += 1
            elif event.key == pygame.K_LEFT:
                position -= 1
                    
        
        #               mouseevent
        if event.type == pygame.MOUSEBUTTONDOWN:
            
            m_btn_down = True
            pos = (pygame.mouse.get_pos() [0], pygame.mouse.get_pos() [1])
            
          

            if btn_magazin[0] <= pos[0] <= (btn_magazin[0]+btn_magazin[2]) and btn_magazin[1] <= pos[1] <= (btn_magazin[1]+btn_magazin[3]):
                light_beam = not light_beam
                if light_beam == True:
                    GPIO.output( 6, GPIO.HIGH)
                    #state_color3 = colours.green
                    #time.sleep(0.2)
                else:
                    GPIO.output( 6, GPIO.LOW)
                    #state_color3 = colours.red
                    #time.sleep(0.2)
                    
            if btn_start[0] <= pos[0] <= (btn_start[0]+btn_start[2]) and btn_start[1] <= pos[1] <= (btn_start[1]+btn_start[3]):
                GPIO.output( 13, GPIO.HIGH) #start

            if btn_stop[0] <= pos[0] <= (btn_stop[0]+btn_stop[2]) and btn_stop[1] <= pos[1] <= (btn_stop[1]+btn_stop[3]):
                GPIO.output( 19, GPIO.HIGH) #Stop

                
        if event.type == pygame.MOUSEBUTTONUP:
            m_btn_down = False
            GPIO.output( 13, GPIO.LOW)
            GPIO.output( 19, GPIO.LOW)

    # GPIO event
    if GPIO.input(14) == True:
        
        sa.move_extensionCylinder_in(True, zylindergeschwindigkeit[1])                
        print(sa.get_state_extensionCylinder())
    else:
        sa.move_extensionCylinder_in(False, zylindergeschwindigkeit[1])                


    if GPIO.input(15) == True:
        sa.move_slewingCylinder_2_nextStation(zylindergeschwindigkeit[0])
        print(sa.get_state_slewingCylinder())

    if GPIO.input(18) == True:
        sa.move_slewingCylinder_2_stackMagazine(zylindergeschwindigkeit[0])
        print(sa.get_state_slewingCylinder())

   
        
    states_ret = (m_btn_down, light_beam, position)
    return states_ret, slewing_arm_runs