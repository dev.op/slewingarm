#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 02:34:27 2020

@author: pi
"""
import pygame
import colours

class button():
    
    def __init__(self, screen, position_x, position_y, btn_colour = colours.dgrey, text_render = None, txt_button = "",
                      text_colour = colours.black, width = 0, height = 0,  font_name ="BackIssuesBB_reg.otf", font_size = 14, margin = 5, shape = 0):
        
        self.screen = screen
        self.position_x = position_x
        self.position_y = position_y
        self.txt_button = txt_button
        self.margin = margin
        self.font_size = font_size
        self.font = pygame.font.Font(font_name, self.font_size)
        self.shape = shape
        
        
        if text_render == None:
            self.text = self.font.render(txt_button, True, text_colour)
        else:
            self.text = text_render
        
        
        self.txt_rect = self.text.get_rect()
        if width == 0:
            self.width = self.txt_rect[2] + 4 * self.margin
        else:
            self.width = width
            
        if shape == 0:
            if height == 0:
                self.height = self.txt_rect[3] + 2 * self.margin
            else:
                self.height = height
        else:                
            if height == 0:
                self.height = self.width
            else:
                self.height = height
                
        self.colour = btn_colour
    
    
   
    def draw(self, txt_button = "", txt_colour = colours.black, text_render = None):
        
        if txt_button == "":
            pass
        else:
            self.text = self.font.render(txt_button, True, txt_colour)
        
        if text_render == None:
            pass
        else:
            self.text = text_render
            
        if self.shape == 0:
            pygame.draw.rect(self.screen, self.colour , (self.position_x,  self.position_y,  self.width, self.height ))
            self.screen.blit(self.text, [self.position_x + 2 * self.margin, self.position_y + self.margin ])
        else:
            pygame.draw.ellipse(self.screen, self.colour , (self.position_x,  self.position_y,  self.width, self.width ))
            self.screen.blit(self.text, [self.position_x + 2 * self.margin, self.position_y + self.width/2 - self.font_size/2 ])
        
        return
    
    def get_rectsize(self):
        return (self.position_x, self.position_y, self.width, self.height)