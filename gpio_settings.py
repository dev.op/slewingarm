#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 23:10:39 2020

@author: pi
"""
import RPi.GPIO as GPIO


def gpio_definition():
    GPIO.setmode(GPIO.BCM)

    GPIO.setup( 26, GPIO.OUT)
    GPIO.setup( 19, GPIO.OUT)
    GPIO.setup( 13, GPIO.OUT)
    GPIO.setup(  6, GPIO.OUT)

#GPIO.setup( 6, GPIO.OUT)
#GPIO.setup( 6, GPIO.OUT)
#GPIO.setup( 6, GPIO.OUT)
#GPIO.setup( 6, GPIO.OUT)


    GPIO.setup(14, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(15, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

    GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(25, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup( 8, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup( 7, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    
    return