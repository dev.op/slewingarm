#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 11:30:18 2020

@author: pi
"""
import pygame
from math import cos, sin, pi


def rotate_image(screen, image, angel_grad, img_height, img_width, pos, distance, show_rect):
    dgrey = ( 105,105,105)
    #distance = 30
    #x_pos = 300
    #y_pos = 300
    #img_width =50
    
    #x_pos = x_pos #+ distance
    #y_pos = y_pos #- distance
    pygame.draw.ellipse(screen, dgrey, [pos[0] , pos[1], 5, 5])
    
    rot_img = pygame.transform.rotate(image, angel_grad)
    angel_rad = angel_grad * (pi/180)
    groesze = rot_img.get_rect()

    #screen.blit(rot_img, [x_pos - groesze.center[0],  y_pos - groesze.center[1]])

    x_0_90 = pos[0] - sin(angel_rad)*(img_width/2)
    y_0_90 = pos[1]   - groesze.height  + cos(angel_rad) * (img_width/2)
    
    x_90_180 = pos[0] + sin(angel_rad)*(img_width/2) - groesze.width
    y_90_180 = pos[1]   - groesze.height  - cos(angel_rad) * (img_width/2)
    
    x_180_270 = pos[0] - groesze.width - sin(angel_rad)*(img_width/2)
    y_180_270 = pos[1] + cos(angel_rad) * (img_width/2)
    
    x_270_360 = pos[0] + sin(angel_rad)*(img_width/2)
    y_270_360 = pos[1] - cos(angel_rad) * (img_width/2)
    
    
    if angel_grad >= 0 and angel_grad <90:
        screen.blit(rot_img, [x_0_90, y_0_90 ])
        if show_rect:
            pygame.draw.rect(screen, dgrey, [x_0_90, y_0_90, groesze.width, groesze.height], 1)
    elif angel_grad >= 90 and angel_grad <180:
        screen.blit(rot_img, [x_90_180, y_90_180  ])
        if show_rect:
            pygame.draw.rect(screen, dgrey, [x_90_180, y_90_180, groesze.width, groesze.height], 1)
    elif angel_grad >= 180 and angel_grad <270:
        screen.blit(rot_img, [x_180_270, y_180_270  ])
        if show_rect:
            pygame.draw.rect(screen, dgrey, [x_180_270, y_180_270, groesze.width, groesze.height], 1)
    else:
        screen.blit(rot_img, [x_270_360, y_270_360  ])
        if show_rect:
            pygame.draw.rect(screen, dgrey, [x_270_360, y_270_360, groesze.width, groesze.height], 1)
    #screen.blit(rechteck_rot, [300, 300 - groesze.center[1]])