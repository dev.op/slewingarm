#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 12 17:27:48 2020

@author: pi
"""
import pygame
import colours
from spacebar import SpaceBar
from button import button
from utilities import rotate_image
from events import events
from event_handler import EventHandler
#import time

#from pygame.locals import *
from math import cos, sin, pi
from slewingarm import slewingarm

sa = slewingarm()
#font = pygame.font.SysFont("comicsansms", 72)
#font = pygame.font.Font("myresources/fonts/Papyrus.ttf", 26)
#font = pygame.font.Font(None, 10)
#font = pygame.font.Font("BackIssuesBB_reg.otf", 10)
#font = pygame.font.Font(None, 20)

#schrift = pygame.font.SysFont('Arial', 10, False, False)
#text = schrift.render("Textinhalt", True, black)

#init pygame
pygame.init()
import txt

window_width = 800
window_height = 480
maszstab = 3
#define colors
#         R    G    B

#gpio_state26  = False
#gpio_state19  = False
#gpio_state13  = False
#gpio_state6  = False

#define fonts
fontsize = 14
#font = pygame.font.SysFont('Arial', fontsize, True, False)
font = pygame.font.Font("BackIssuesBB_reg.otf", fontsize)
#schrift = pygame.font.SysFont('Arial', 14, False, False)
font_interface = pygame.font.Font("BackIssuesBB_reg.otf", 24)




#distance = 0
#pos_bevor = (0, 0)
#pos_diff = (0, 0)
#diff = 0
#new = 0
position = 0
#btn_down = False

rotation_position = 180
#richtung = 1
zylindergeschwindigkeit_max = 10
zylindergeschwindigkeit = (0, 0)
#prozent = 0
FPS = 60
#Ausschiebezylinder_bottom_position = 0
workpiece_top_position = 268
wp_linear = True
light_beam = False
wp_move = False
wp_rotate = False
btn_start = False

# Fenster öffnen

screen = pygame.display.set_mode((window_width, window_height))
pygame.display.set_caption("Schwenkarm")



# Schwenkzylinder
Schwenkzylinder = pygame.image.load("Teil1_c.PNG").convert_alpha()
Schwenkzylinder_rect = Schwenkzylinder.get_rect()
Schwenkzylinder_size = (Schwenkzylinder_rect.width, Schwenkzylinder_rect.height)
breite = int(Schwenkzylinder_size[0]/maszstab)
Schwenkzylinder_size = (breite, int(Schwenkzylinder_size[1]*breite/Schwenkzylinder_size[0]))
Schwenkzylinder_pos = (300, 341)
Schwenkzylinder = pygame.transform.scale(Schwenkzylinder, Schwenkzylinder_size)
#Schwenkzylinder = pygame.transform.scale(Schwenkzylinder, (50,200))

Auflage = pygame.image.load("Teil1_b.PNG").convert_alpha()
Auflage_rect = Auflage.get_rect()
Auflage_size = (Auflage_rect.width, Auflage_rect.height)
breite_a = int(Auflage_size[0]/maszstab)
Auflage_size = (breite_a, int(Auflage_size[1]*breite_a/Auflage_size[0]))
Auflage_pos = (110, 80)
Auflage = pygame.transform.scale(Auflage, Auflage_size)

Ausschiebezylinder = pygame.image.load("Teil1_a.png").convert_alpha()
Ausschiebezylinder_rect = Ausschiebezylinder.get_rect()
Ausschiebezylinder_size = (Ausschiebezylinder_rect.width, Ausschiebezylinder_rect.height)
breite_b = int(Ausschiebezylinder_size[0]/maszstab)
Ausschiebezylinder_size = (breite_b, int(Ausschiebezylinder_size[1]*breite_b/Ausschiebezylinder_size[0]))
Ausschiebezylinder_pos = (125, 80)

Ausschiebezylinder = pygame.transform.scale(Ausschiebezylinder, Ausschiebezylinder_size)

sb_schwenkzylinder = SpaceBar(screen, 590, 130, 150, 15)
sb_ausschiebezylinder = SpaceBar(screen, 590, 170, 150, 15)

btn_magazin = button(screen, 585,205, colours.black, txt.magazin_voll)
btn_start = button(screen, 585,245, colours.green, txt.btn_start)
btn_stop = button(screen, 585, 285, colours.red, txt.btn_stop, shape = 1)

button_rects = (btn_magazin.get_rectsize(), btn_start.get_rectsize(), btn_stop.get_rectsize())

states = (light_beam, position)
eh = EventHandler(sa, states, button_rects)


clock = pygame.time.Clock()


###########################################################
#                        main loop
###########################################################
slewing_arm_runs = True


while slewing_arm_runs:
    

###########################################################
#                        eventhandling
###########################################################
    

    #states = (btn_down, light_beam, position)
    #states = (light_beam, position)
    slewing_arm_runs = eh.update_events(zylindergeschwindigkeit)
    #states, slewing_arm_runs = events(sa, zylindergeschwindigkeit, states, button_rects)
    #btn_down = states[0]
    #light_beam = states[1]
    light_beam = eh.get_state_light_beam()
    btn_down = eh.get_state_m_btn_down()
    position = eh.get_position()
    #btn_start = states[3]
    
    
    # Spiellogik hier integrieren
        
    rotation_position = 180 - 1.8*sa.get_state_slewingCylinder()
    extend_position = 1.3 * sa.get_state_extensionCylinder()

    Ausschiebezylinder_bottom_position = Ausschiebezylinder_pos[1] + (extend_position) + Ausschiebezylinder_rect.height / maszstab
    if Ausschiebezylinder_bottom_position >= workpiece_top_position and not light_beam:
        workpiece_top_position = Ausschiebezylinder_bottom_position

    if round(workpiece_top_position) == 320:       
        wp_linear = False
    if round(rotation_position) == 180:
        wp_rotate = True
    
    if not light_beam:
        if Ausschiebezylinder_bottom_position <= 268:
            wp_magazin = (110, 268, 42, 42)
        else:
            wp_move = True        
    else:
        wp_magazin = (110, 268, 0, 0)
    
    if wp_move:
        if wp_linear:
            wp_position = (110, workpiece_top_position, 42, 42)
        elif wp_rotate:
            wp_position =  (280 + 170 * cos(rotation_position* (pi/180)), 320 - 170 * sin(rotation_position* (pi/180)), 42, 42)
        
    else:
        wp_position = (110, 268, 0, 0)
        

    
    spacebar_schwenkzylinder = sb_schwenkzylinder.percente(btn_down)
    
    spacebar_ausschiebezylinder = sb_ausschiebezylinder.percente(btn_down)
    
    
    zylindergeschwindigkeit_schwenkzylinder = spacebar_schwenkzylinder * zylindergeschwindigkeit_max
    zylindergeschwindigkeit_ausschiebezylinder = spacebar_ausschiebezylinder * zylindergeschwindigkeit_max
    zylindergeschwindigkeit = (zylindergeschwindigkeit_schwenkzylinder, zylindergeschwindigkeit_ausschiebezylinder)
    
###########################################################
#                        sensorik
###########################################################
#austaster
#starttaster
#magazinprintrelais

#sa.get_state_extensionCylinder
#sa.get_state_slewingCylinder
#sa.get_vacuum_on

###########################################################
#                        logik textausgabe
###########################################################
    
    txt_sb_schwenkzylinder = font.render("Schwenkzylinder: " + str(round(spacebar_schwenkzylinder*100)) + "%", True, colours.black)
    txt_sb_ausschiebezylinder = font.render("Ausschiebezylinder: " + str(round(spacebar_ausschiebezylinder*100)) + "%", True, colours.black)

    if light_beam:
        txt_magazin = txt.magazin_leer
    else:
        txt_magazin = txt.magazin_voll
        
    

    
    
    # Spielfeld löschen
    
    screen.fill(colours.lgrey)
    pygame.draw.line(screen, colours.dgrey, [0.7*window_width, 0.87*window_height], [0.7*window_width, (1 - 0.87)*window_height], 4)
    pygame.draw.line(screen, colours.dgrey, [0, 0.87*window_height], [window_width, 0.87*window_height], 4)
    pygame.draw.line(screen, colours.dgrey, [0, (1 - 0.87)*window_height], [window_width, (1 - 0.87)*window_height], 4)

    #pygame.draw.rect(screen, cyan , (200,  200,  10,  30))

    #(self.colour , (self.position_x,  self.position_y,  self.new, self.height))
    #pygame.draw.rect(screen, draw1[0], draw1[1])
    #pygame.draw.rect(screen, draw2)
    sb_schwenkzylinder.draw()
    sb_ausschiebezylinder.draw()
    # Spielfeld/figuren zeichnen
    rotate_image(screen, Schwenkzylinder, rotation_position, Schwenkzylinder_size[0],Schwenkzylinder_size[1], Schwenkzylinder_pos, 0, False)
    screen.blit(Auflage, [Auflage_pos[0], Auflage_pos[1]])
    screen.blit(Ausschiebezylinder, [Ausschiebezylinder_pos[0], Ausschiebezylinder_pos[1] + (extend_position)])
    
    #pygame.draw.ellipse(screen, colours.black, (110, 320, 42, 42))
    
    pygame.draw.ellipse(screen, colours.black, wp_magazin)
    
    #if wp_rotation:
    #    pygame.draw.ellipse(screen, colours.black, (280 + 170 * cos(rotation_position* (pi/180)), 320 - 170 * sin(rotation_position* (pi/180)), 42, 42))
    #elif not light_beam:
    #    pygame.draw.ellipse(screen, colours.black, (110, workpiece_top_position, 42, 42))
    pygame.draw.ellipse(screen, colours.black, wp_position)
    #pygame.draw.ellipse(screen, colours.black, (110 + 170 * cos(rotation_position* (pi/180)), 320 - 170 * sin(rotation_position* (pi/180)), 42, 42))
###########################################################
#                        widgets
###########################################################
    
    #pygame.draw.rect(screen, colours.black , (585, 205,  195,  25))
    #pygame.draw.rect(screen, colours.green , (585, 245,  65,  25))
    #pygame.draw.ellipse(screen, colours.red , (585, 285,  55,  55))
    
    sb_schwenkzylinder.draw()
    sb_ausschiebezylinder.draw()
    
    btn_magazin.draw(text_render=txt_magazin)
    btn_start.draw()
    btn_stop.draw()
###########################################################
#                        text dynamisch
###########################################################
    screen.blit(txt_sb_schwenkzylinder, [585, 110])
    screen.blit(txt_sb_ausschiebezylinder, [585, 150])    
    
###########################################################
#                        text statisch
###########################################################
    
    screen.blit(txt.geschwindigkeit, [570, 85]) #+   
    #screen.blit(txt_magazin, [590, 210])
    #screen.blit(txt.btn_start, [590, 250])
    #screen.blit(txt.btn_stop, [590, 305])


    Abstand = 100

    
    screen.blit(txt.input_0_0, [10, 16])
    screen.blit(txt.input_0_1, [10 + 1*Abstand, 16])
    screen.blit(txt.input_0_2, [10 + 2*Abstand, 16])
    screen.blit(txt.input_0_3, [10 + 3*Abstand, 16])
    
    screen.blit(txt.input_0_4, [10 + 4*Abstand, 16])
    screen.blit(txt.input_0_5, [10 + 5*Abstand, 16])
    screen.blit(txt.input_0_6, [10 + 6*Abstand, 16])
    screen.blit(txt.input_0_7, [10 + 7*Abstand, 16])
    
    screen.blit(txt.output_0_0, [10, 440])
    screen.blit(txt.output_0_1, [10 + 1*Abstand, 440])
    screen.blit(txt.output_0_2, [10 + 2*Abstand, 440])
    screen.blit(txt.output_0_3, [10 + 3*Abstand, 440])
    
    screen.blit(txt.output_0_4, [10 + 4*Abstand, 440])
    screen.blit(txt.output_0_5, [10 + 5*Abstand, 440])
    screen.blit(txt.output_0_6, [10 + 6*Abstand, 440])
    screen.blit(txt.output_0_7, [10 + 7*Abstand, 440])
    

###########################################################
#                        monitoring
###########################################################
        
    print(zylindergeschwindigkeit_schwenkzylinder)
    print(zylindergeschwindigkeit_ausschiebezylinder)
    print(zylindergeschwindigkeit)
    print(zylindergeschwindigkeit_max)
    print(Auflage_size)
    print(rotation_position)
    print(round(Ausschiebezylinder_bottom_position))
    print(position)
    print(btn_magazin.get_rectsize())
    a = button_rects[0]
    print(button_rects[0])
    #pygame.display.flip()
    pygame.display.update()
    # Refresh-Zeiten festlegen
    clock.tick(FPS)
    
    sa.monitoring()
    
pygame.quit()