#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 10:19:36 2020

@author: pi
"""
import pygame
import RPi.GPIO as GPIO



class EventHandler():
    def __init__(self, sa, states, button_rects):
        
        self.gpio_definition()
        self.sa = sa
        self.btn_rect_magazin = button_rects[0]
        self.btn_rect_start = button_rects[1]
        self.btn_rect_stop = button_rects[2]
        
        self.m_btn_down = False
        self.light_beam = states[0]
        self.position = states[1]
        
        self.slewing_arm_runs = True
        


##########################################################################################################################
        
    def update_events(self, zylindergeschwindigkeit):
        for event in pygame.event.get():
            self.key_events(event)
            self.mouse_events(event, zylindergeschwindigkeit)
        return self.slewing_arm_runs


##########################################################################################################################
            
    def key_events(self, event):
        
        
        if event.type == pygame.QUIT:
            self.slewing_arm_runs = False
            print("Spieler hat Quit-Button angeklickt")
                
        elif event.type == pygame.KEYDOWN:
            print("Spieler hat Taste gedrückt")
            if event.key == pygame.K_RIGHT:
                self.position += 1
                pass
            elif event.key == pygame.K_LEFT:
                self.position -= 1
                pass
        return


##########################################################################################################################
                    
    def mouse_events(self, event, zylindergeschwindigkeit):
        #gpio_definition()

        if event.type == pygame.MOUSEBUTTONDOWN:

            self.m_btn_down = True
            pos = (pygame.mouse.get_pos() [0], pygame.mouse.get_pos() [1])


###########################################################################
#                            button_magazin
###########################################################################
            
            in_x_borders_magazin = self.btn_rect_magazin[0] <= pos[0] <= (self.btn_rect_magazin[0] + self.btn_rect_magazin[2])
            in_y_borders_magazin = self.btn_rect_magazin[1] <= pos[1] <= (self.btn_rect_magazin[1] + self.btn_rect_magazin[3])
            if in_x_borders_magazin and in_y_borders_magazin:
                self.light_beam = not self.light_beam
                if self.light_beam == True:
                    GPIO.output( 6, GPIO.HIGH)
                else:
                    GPIO.output( 6, GPIO.LOW)


###########################################################################
#                            button_start
###########################################################################

            in_x_borders_start = self.btn_rect_start[0] <= pos[0] <= (self.btn_rect_start[0] + self.btn_rect_start[2])
            in_y_borders_start = self.btn_rect_start[1] <= pos[1] <= (self.btn_rect_start[1] + self.btn_rect_start[3])
            if in_x_borders_start and in_y_borders_start:
                GPIO.output( 13, GPIO.HIGH) #start


###########################################################################
#                            button_start
###########################################################################

            in_x_borders_stop = self.btn_rect_stop[0] <= pos[0] <= (self.btn_rect_stop[0] + self.btn_rect_stop[2])
            in_y_borders_stop = self.btn_rect_stop[1] <= pos[1] <= (self.btn_rect_stop[1] + self.btn_rect_stop[3])
            if in_x_borders_stop and in_y_borders_stop:
                GPIO.output( 19, GPIO.HIGH) #start
                
        if event.type == pygame.MOUSEBUTTONUP:
            self.m_btn_down = False
            GPIO.output( 13, GPIO.LOW)
            GPIO.output( 19, GPIO.LOW)
            
        return
    
        if GPIO.input(14) == True:
        
            self.sa.move_extensionCylinder_in(True, zylindergeschwindigkeit[1])                
            print(self.sa.get_state_extensionCylinder())
        else:
            self.sa.move_extensionCylinder_in(False, zylindergeschwindigkeit[1])                


        if GPIO.input(15) == True:
            self.sa.move_slewingCylinder_2_nextStation(zylindergeschwindigkeit[0])
            print(self.sa.get_state_slewingCylinder())

        if GPIO.input(18) == True:
            self.sa.move_slewingCylinder_2_stackMagazine(zylindergeschwindigkeit[0])
            print(self.sa.get_state_slewingCylinder())



##########################################################################################################################
            
    def get_state_m_btn_down(self):
        return self.m_btn_down
    def get_state_light_beam(self):
        return self.light_beam
    def get_position(self):
        return self.position

##########################################################################################################################
            
    def gpio_definition(self):
        
        GPIO.setmode(GPIO.BCM)
        
        
###########################################################################
#                            GPIO-Output
###########################################################################
        
        GPIO.setup( 26, GPIO.OUT)
        GPIO.setup( 19, GPIO.OUT)
        GPIO.setup( 13, GPIO.OUT)
        GPIO.setup(  6, GPIO.OUT)


###########################################################################
#                            GPIO-Input
###########################################################################

        GPIO.setup(14, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(15, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

        GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(25, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup( 8, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup( 7, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    
        return