import time
from os import system

class slewingarm:

    def __init__(self):
        self.reset()        

    def move_extensionCylinder_in(self, state, velocity):
        
        if state == True:
            if self.state_extensionCylinder < 100:
                self.state_extensionCylinder += 1*velocity
        elif self.state_extensionCylinder > 0:
            self.state_extensionCylinder -= 1*velocity

        if self.state_extensionCylinder == 100:
            self.extensionCylinder_in = True
        elif self.state_extensionCylinder == 0:
            self.extensionCylinder_out = True
        else:
            self.extensionCylinder_out = False
            self.extensionCylinder_in = False
        
        if self.state_extensionCylinder >= 100:
            self.state_extensionCylinder = 100
            
        if self.state_extensionCylinder <= 0:
            self.state_extensionCylinder = 0
        
        self.monitoring()    
        return
    
    def get_state_extensionCylinder(self):
        return self.state_extensionCylinder


    def move_slewingCylinder_2_stackMagazine(self, velocity):
        if self.state_slewingCylinder > 0:
            self.state_slewingCylinder -= 1*velocity
        self.update_slewingCylinderSensors()
        
        if self.state_slewingCylinder <= 0:
            self.state_slewingCylinder = 0

        return
    
    def move_slewingCylinder_2_nextStation(self, velocity):
        if self.state_slewingCylinder < 100:
            self.state_slewingCylinder += 1*velocity
        self.update_slewingCylinderSensors()
        
        if self.state_slewingCylinder >= 100:
            self.state_slewingCylinder = 100

        return
    
    def update_slewingCylinderSensors(self):
        if self.state_slewingCylinder == 100:
            self.slewingCylinder_at_nextStation = True
        elif self.state_slewingCylinder == 0:
            self.slewingCylinder_at_stackMagazine = True
        else:
            self.slewingCylinder_at_nextStation = False
            self.slewingCylinder_at_stackMagazine = False
            
        self.monitoring()
    
    def get_state_slewingCylinder(self):
        return self.state_slewingCylinder
        
    def set_vacuum(self, state):
        self.vacuum_on = True
        return

    def set_vacuum_blow(self, state):
        pass
        return

    def set_sgnLamp(self, state):
        pass
        return
    
    def get_vacuum_on(self):
        return self.vacuum_on

    def reset(self):
        self.btn_stop = True
        self.btn_start = False
        self.magazine_empty = False
        self.slewingCylinder_at_nextStation = False
        self.slewingCylinder_at_stackMagazine = True
        self.extensionCylinder_in = False
        self.extensionCylinder_out = True
        self.vacuum_on = False
        
        self.state_extensionCylinder = 0
        self.state_slewingCylinder = 0
        
        self.monitoring()
        

    def monitoring(self):#
        
        #system('clear') 
        print('btn_stop: ' + str(self.btn_stop))
        print('')
        print('extensionCylinder_in: ' + str(self.extensionCylinder_in))
        print('extensionCylinder_out: ' + str(self.extensionCylinder_out))
        print('extensionCylinder_state: ' + str(self.state_extensionCylinder))
        
        print('')
        print('slewingCylinder_at_stackMagazine: ' + str(self.slewingCylinder_at_stackMagazine))
        print('slewingCylinder_at_nextStation: ' + str(self.slewingCylinder_at_nextStation))
        print('slewingCylinder_state: ' + str(self.state_slewingCylinder))
        print('')
