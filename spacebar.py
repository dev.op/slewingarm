#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 11:22:43 2020

@author: pi
"""
import pygame

class SpaceBar():
    
    def __init__(self, screen, position_x, position_y, width = 100, height = 20, lineWidth = 2, colour = (105, 105, 105)):
        self.screen = screen
        self.position_x = position_x
        self.position_y = position_y
        self.width = width #100
        self.height = height #20 
        self.lineWidth = lineWidth #2
        self.new = 0
        self.percent = 0
        self.colour = colour #dgrey
  
    
    def percente(self, move_sb):
        if move_sb:
            in_border_x = self.position_x <= pygame.mouse.get_pos() [0] <= self.position_x + self.width
            in_border_y = self.position_y <= pygame.mouse.get_pos() [1] <= self.position_y + self.height
                  #pygame.display.update()
            if in_border_x and in_border_y:
                position = pygame.mouse.get_pos() [0]
                self.new = position - self.position_x
        self.percent = self.new / (self.position_x + self.width - self.position_x)
        return self.percent
    
    def draw(self):
        frame = ( self.position_x,  self.position_y, self.width, self.height)
        pygame.draw.rect(self.screen, self.colour , frame, self.lineWidth)
        pygame.draw.rect(self.screen, self.colour , (self.position_x,  self.position_y,  self.new, self.height))
        return
    