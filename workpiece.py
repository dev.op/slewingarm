#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 13:36:21 2020

@author: pi
"""

import pygame

class WorkPiece():
    
    def __init__(self):
  
    
    def percente(self, move_sb):
        if move_sb:
            in_border_x = self.position_x <= pygame.mouse.get_pos() [0] <= self.position_x + self.width
            in_border_y = self.position_y <= pygame.mouse.get_pos() [1] <= self.position_y + self.height
                  #pygame.display.update()
            if in_border_x and in_border_y:
                position = pygame.mouse.get_pos() [0]
                self.new = position - self.position_x
        self.percent = self.new / (self.position_x + self.width - self.position_x)
        return self.percent
    
    def draw(self):
        frame = ( self.position_x,  self.position_y, self.width, self.height)
        pygame.draw.rect(self.screen, self.colour , frame, self.lineWidth)
        pygame.draw.rect(self.screen, self.colour , (self.position_x,  self.position_y,  self.new, self.height))
        return